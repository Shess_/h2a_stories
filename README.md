# H2A Stories
<pre>
 ___  ___    _______  ________     
|\  \|\  \  /  ___  \|\   __  \    
\ \  \\\  \/__/|_/  /\ \  \|\  \   
 \ \   __  \__|//  / /\ \   __  \  
  \ \  \ \  \  /  /_/__\ \  \ \  \ 
   \ \__\ \__\|\________\ \__\ \__\
    \|__|\|__| \|_______|\|__|\|__|
</pre>
## Description
Programme pour l'installation Stories réaliser dans le cadre de l'exposition Human After All.


## Installation
# Matériel:
- Un PC
- Une webcam
- imprimante thermique EPSON (uniquement testé avec une imprimante TM-T20II)

# Dépendances:
- [opencv](https://pypi.org/project/opencv-python/)
- [numpy](https://pypi.org/project/numpy/)
- [PIL](https://pypi.org/project/Pillow/)
- [pygame](https://pypi.org/project/pygame/)
- [fastai](https://pypi.org/project/fastai/)

# Instructions
- /main.py ligne 32: indiquer le numéro de la caméra à utiliser (0 par défaut).
- /text_gen.py ligne 45: remplacer YOUR_KEY par un token de connection valide pour l'Inference API d'huggingface.
- /thermic_printer.py lignes 12 et 13: remplacer YOUR_ID_VENDOR et YOUR_ID_PRODUCT en suivant les instructions disponibles [ici](https://github.com/benoitguigal/python-epson-printer).


## Usage
    python main.py

## Auteurs et remerciements
- [Franck Miquel](https://twitter.com/Shess1735) (développeur)
- [Queenie-Flavie Charles](http://flaviecharles.fr/) (graphic designer)

Merci à [Benoît Guigal](https://benoitguigal.fr/) pour [sa bibliothèque](https://github.com/benoitguigal/python-epson-printer) python permettant de contrôler les imprimantes thermiques EPSON.
