from fastai.vision.all import *
import gc

learn = load_learner('./fashion-attribute-recognition.pkl')

def predict_attribute(model, path, display_img=True):
    predicted = model.predict(path)
    if display_img:
        size = 244,244
        img=Image.open(path)
        img.thumbnail(size,Image.ANTIALIAS)
        #display(img)

    return predicted[0]

def get_fashion_attributes(image_path):
    attributes = predict_attribute(learn, image_path)
    print(attributes)
    return attributes
