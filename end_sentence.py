
def replace_ending(sentence, old, new):
    if sentence.endswith(old):
        return sentence[:-len(old)] + new
    return sentence

def to_sent(text):
    comma_split = text.split(',')
    if len(comma_split) > 1:
        comma_split.pop()
        text = ','.join(comma_split)
        text += ','
    sent = replace_ending(text, ',', '.')
    return sent

def remove_uncomplete_sent(text):
    if not text.endswith('.'):
        dot_split = text.split('.')
        if len(dot_split) > 1:
            dot_split.pop()
            text = '.'.join(dot_split)
            text += '.'

    return text

if __name__ == '__main__':

    sentences = [
    "Tu as été un homme de bien, tu as fait de ton mieux pour aider les autres,",
    "tu as été un bon père, un bon mari, un bon ami, un bon voisin, un",
    "bon citoyen, un bon employé, un bon patron, un bon citoyen, un bon voisin, un",
    ]

    for sent in sentences:
        print('///////')
        print(to_sent(sent))
