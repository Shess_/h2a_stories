
import csv

csv_file_name = "keywords_dict.csv"
keywords_dict = {}

with open(csv_file_name) as csvDataFile:
    csv_reader = csv.reader(csvDataFile, delimiter=',')
    for i, row in enumerate(csvDataFile):
        row = row.split(',')
        if not len(row) == 0:
            fashion_attribute = row[0]
            keywords = row[1:]

            white_space = [" ", "\t", "\n"]
            for i, keyword in enumerate(keywords):
                keywords[i] = "".join([c for c in keyword if c not in white_space])

            keywords = [keyword for keyword in keywords if keyword != ""]

            keywords_dict[fashion_attribute] = keywords


def get_keywords(fashion_attributes):

    keywords = []
    for fashion_attribute in fashion_attributes:
        keywords+=keywords_dict[fashion_attribute]

    return keywords

if __name__ == '__main__':
    print(get_keywords(["colorblock", "everyday", "cargo", "dotted"]))
