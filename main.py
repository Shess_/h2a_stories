import sys
import logging
import threading
import time

import cv2 as cv
import numpy as np
from PIL import Image
import pygame
from pygame.locals import *

print("loading fashion model.")
from custom_learner import *
from fashion_attributes import get_fashion_attributes
print("done")

from user_picture import get_usr_picture

print("load keywords")
from keywords import get_keywords
print("done.")
from text_gen import get_story
print("accessing thermic printer.")
from thermic_printer import print_story
print("done.")

# initialize the camera
# If you have multiple camera connected with
# current device, assign a value in cam_port
# variable according to that
print("accessing camera.")
cam_port = #YOUR CAM PORT
cam = cv.VideoCapture(cam_port)
print("done.")

if cam.isOpened(): # try to get the first frame
    rval, frame = cam.read()
else:
    print("no camera detected !")
    quit()


# pygame init
pygame.init()
FPS = 60
fpsClock = pygame.time.Clock()
WINDOW_WIDTH = 1920
WINDOW_HEIGHT = 1080
WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT), pygame.FULLSCREEN)
pygame.display.set_caption("test interface stories")
BACKGROUND = (0,0,0)

#####

def cvImageToSurface(cvImage): #https://stackoverflow.com/questions/64183409/how-do-i-convert-an-opencv-image-bgr-and-bgra-to-a-pygame-surface-object
    if cvImage.dtype.name == 'uint16':
        cvImage = (cvImage / 256).astype('uint8')
    size = cvImage.shape[1::-1]
    if len(cvImage.shape) == 2:
        cvImage = np.repeat(cvImage.reshape(size[1], size[0], 1), 3, axis = 2)
        format = 'RGB'
    else:
        format = 'RGBA' if cvImage.shape[2] == 4 else 'RGB'
        cvImage[:, :, [0, 2]] = cvImage[:, :, [2, 0]]
    surface = pygame.image.frombuffer(cvImage.flatten(), size, format)
    return surface.convert_alpha() if format == 'RGBA' else surface.convert()

def get_cam_preview():
    rval, frame = cam.read()
    return cvImageToSurface(frame)

#####

class Animation(object):
    """docstring for Animation."""

    def __init__(self, frames_list, frames_durations):
        super(Animation, self).__init__()
        self.frames_list = frames_list
        self.frames_durations = frames_durations
        self.last_update_date = 0
        self.current_frame_id = 0
        self.finished = True
        self.loop = False

    def start(self, loop=False):
        self.loop = loop
        self.last_update_date = time.time()
        self.current_frame_id = 0
        self.finished = False

    def update(self):
        if time.time()-self.last_update_date >= self.frames_durations[self.current_frame_id]:
            self.current_frame_id += 1

            if self.loop:
                if self.current_frame_id == len(self.frames_list):
                    self.current_frame_id = 0
            else:
                if self.current_frame_id == len(self.frames_list)-1:
                    self.finished = True

            self.last_update_date = time.time()

    def get_current_frame(self):
        return self.frames_list[self.current_frame_id]

def get_anim_from_gif(gif_path):
    frames_list = []
    frames_durations = []
    gif_img = Image.open(gif_path)
    for i in range(gif_img.n_frames):
        gif_img.seek(i)
        frames_durations.append(gif_img.info['duration']/1000)
        img = gif_img.convert("RGBA")
        data = img.tobytes("raw", "RGBA")
        frames_list.append(pygame.image.fromstring(data, img.size, "RGBA"))

    return Animation(frames_list, frames_durations)

#####

CURRENT_INTERFACE_ALIVE = True

def task1(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    loop = True
    while loop:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    loop = False

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False

picture_path = None
def task2(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    global cam
    global picture_path
    picture_path = get_usr_picture(cam)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False

fashion_attributes = None
def task3(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    global picture_path
    global fashion_attributes
    fashion_attributes = get_fashion_attributes(picture_path)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False


keywords = None
def task4(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    global fashion_attributes
    global keywords
    keywords = get_keywords(fashion_attributes)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False

story = None
def task5(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    global keywords
    global story
    story = get_story(keywords)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False

def task6(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    print_story(story)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False

def task7(name):
    global CURRENT_INTERFACE_ALIVE
    logging.info("starting %s", name)

    time.sleep(5)

    logging.info("finishing %s", name)
    CURRENT_INTERFACE_ALIVE = False


def get_animation_frame(animation):
    if animation.finished:
        animation.start(loop=True)
        return animation.get_current_frame()

    animation.update()
    return animation.get_current_frame()

ANIM_PATH = "anim/"

animation1 = get_anim_from_gif(ANIM_PATH+"ecran1-anim-v2.gif")
def interface_getter_1():
    return get_animation_frame(animation1)

animation2 = get_anim_from_gif(ANIM_PATH+"fond-noir.gif")
overlay_anim = get_anim_from_gif(ANIM_PATH+"ecran2-noir-v2.gif")
def interface_getter_2():
    anim_frame = get_animation_frame(animation2)
    cam_preview_frame = get_cam_preview()
    overlay_frame = get_animation_frame(overlay_anim)
    cam_preview_frame.blit(pygame.transform.smoothscale(overlay_frame, (640, 480)), (0, 0))
    anim_frame.blit(cam_preview_frame, (1920//2-640//2, 1080//-480//2))
    return anim_frame

animation3 = get_anim_from_gif(ANIM_PATH+"computing_anim.gif")
def interface_getter_3():
    return get_animation_frame(animation3)

animation4 = get_anim_from_gif(ANIM_PATH+"computing_anim.gif")
def interface_getter_4():
    return get_animation_frame(animation4)

animation5 = get_anim_from_gif(ANIM_PATH+"computing_anim.gif")
def interface_getter_5():
    return get_animation_frame(animation5)

animation6 = get_anim_from_gif(ANIM_PATH+"printing_anim.gif")
def interface_getter_6():
    return get_animation_frame(animation6)

animation7 = get_anim_from_gif(ANIM_PATH+"goodbye_anim.gif")
def interface_getter_7():
    return get_animation_frame(animation7)


format = "%(asctime)s: %(message)s"
logging.basicConfig(format=format, level=logging.INFO,
                    datefmt="%H:%M:%S")

while True:
    thread1 = threading.Thread(target=task1, args=("task 1",))
    thread2 = threading.Thread(target=task2, args=("task 2",))
    thread3 = threading.Thread(target=task3, args=("task 3",))
    thread4 = threading.Thread(target=task4, args=("task 4",))
    thread5 = threading.Thread(target=task5, args=("task 5",))
    thread6 = threading.Thread(target=task6, args=("task 6",))
    thread7 = threading.Thread(target=task7, args=("task 7",))

    task_list = [(thread1, interface_getter_1),
                (thread2, interface_getter_2),
                (thread3, interface_getter_3),
                (thread4, interface_getter_4),
                (thread5, interface_getter_5),
                (thread6, interface_getter_6),
                (thread7, interface_getter_7)]


    for thread, interface_getter in task_list:
        CURRENT_INTERFACE_ALIVE = True

        thread.start()

        while CURRENT_INTERFACE_ALIVE:
            for event in pygame.event.get():
                if event.type == QUIT:
                    pygame.quit()
                    sys.exit()

            # Render elements
            WINDOW.fill(BACKGROUND)

            WINDOW.blit(interface_getter(),(0, 0))

            pygame.display.update()
            fpsClock.tick(FPS)


        thread.join()
