import time

import cv2 as cv

def get_usr_picture(cam, preview_duration = 5):

    time.sleep(preview_duration)

    # reading the input using the camera
    result, image = cam.read()

    #show error msg while no result is found
    while not result:
        input("No image detected. Please try again!")
        # reading the input using the camera
        result, image = cam.read()

    picture_path = "./usr_temp.png"
    # saving image in local storage
    cv.imwrite(picture_path, image)

    return picture_path


if __name__ == '__main__':
    print(get_usr_picture())
