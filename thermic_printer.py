# -*- coding: UTF-8 -*-
from epsonprinter import EpsonPrinter
from optparse import OptionParser
import sys

# source: https://github.com/benoitguigal/python-epson-printer
# usage exemple:
# lsusb
# Bus 001 Device 007: ID 04b8:0e15 Seiko Epson Corp. TM-T20II
# id_vendor: 0x04b8 id_product: 0x0e15

id_vendor = #YOUR ID VENDOR
id_product = #YOUR ID PRODUCT

printer = EpsonPrinter(id_vendor, id_product)

def print_story(story):

    formated_story = story.encode('cp437',"ignore")

    printer.linefeed()
    printer.print_text(formated_story)
    printer.linefeed()
    printer.linefeed()
    printer.linefeed()
    printer.linefeed()
    printer.linefeed()
    printer.cut()

if __name__ == '__main__':
    story = "Les androïdes rêvent-ils de moutons électriques ?"
    print_story(story)
