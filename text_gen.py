import requests
import random
import re
import string

import end_sentence

banned_words = ["viol", "violer", "violé", "violée", "violées", "violés", "violeur", "violeurs"]

def check_banned_words(banned_words, sentence):
    sentence = sentence.translate(str.maketrans('', '', string.punctuation))
    word_list = sentence.split(' ')

    for banned_word in banned_words:
        if banned_word in word_list:
            return True

    return False

def get_random_intro():
	intros = [  "moi le grand devin, ",
                "moi l'ordinateur magique, ",
                "moi le vieux sage, ",
                "moi l'inconnu, ",
                "moi le savant, ",
                "moi l'IA du futur, "]

	return random.choice(intros)

def get_prompt(keywords): #randomly generated because "use_cache": False don't work
    prompt = ""
    prompt += get_random_intro() #just for fun
    prompt += "je vais te donner ton horoscope de personne"

    for i, keyword in enumerate(keywords):
        if len(keywords)>1 and i == len(keywords)-1:
            prompt += " et " + keyword
        else:
            prompt += ", " + keyword

    prompt += ":"
    return prompt

API_URL = "https://api-inference.huggingface.co/models/bigscience/bloom-560m"
headers = {"Authorization": "Bearer YOUR_KEY"}

def query(payload):
	response = requests.post(API_URL, headers=headers, json=payload)
	return response.json()

def call_model(prompt):

    output = query({
    	"inputs":  prompt,
        "parameters": {"max_new_tokens": 100, "repetition_penalty": 60.0},
        "options": {"use_cache": False, "wait_for_model": True}
    })
    print(output)
    output = output[0]["generated_text"][len(prompt):] #"return_full_text": False don't work

    return output

def is_ban(story_part):
    return check_banned_words(banned_words, story_part)

def get_story(keywords):
    prompt = get_prompt(keywords)
    print(prompt)
    story = call_model(prompt)

    while is_ban(story):
        story = call_model(prompt)

    return story

if __name__ == '__main__':
    while True:
        keywords = ["heureux", "droit", "honnête"]
        print(get_story(keywords))
        input()
